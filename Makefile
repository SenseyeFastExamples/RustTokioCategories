.PHONY: up down categories ps logs
up:
	sudo docker-compose up -d
down:
	sudo docker-compose down
ps:
	sudo docker ps -a
categories:
	sudo docker exec -it rust_tokio_categories bash
logs:
	sudo docker logs rust_tokio_categories --tail=50 -f